[![pipeline status](https://gitlab.com/contentascode/activist-apprentice-course-template/badges/master/pipeline.svg)](https://gitlab.com/smallworldnews/safely-securely-producing-media/commits/master)

*The look and feel of this course are actively being developed.*

The beta expo app is available here: https://expo.io/@stevewyshy/safely-securely-producing-media

or you can scan this QR code:
![QR Code](content/images/safely-securely-expo-QR.png)

# SAFELY & SECURELY PRODUCING MEDIA

There are many great guides to help you securely send media files online. But we find they are mostly text-heavy instructions for dealing with technical or security concerns surrounding the posting of content online. In response, we’ve decided to create a guide combining this information with a clear visual walk through on producing high-quality media. Our primary goal is to not only teach you how to share your story with the world safely, but also help you clarify what that story is and communicate it more effectively.

This particular instance of the guide is being implemented as a fork of the the [**Content as Code**](https://github.com/iilab/contentascode) project from [iilab](http://iilab.org).

## HOW THE GUIDE WORKS

<img src="content/images/stop.png" alt="stop" width="75"></img>

The guide is split up into three sections: [Plan Your Story](/content/1-plan-for-safety/index.md), [Record Your Story](/content/5-recording-safely/index.md), and [Share Your Story](/content/1-plan-for-safety/index.md). It is designed to instruct someone who has never taken a photo or shot video before through the entire process of telling a story with these media. It is also for anyone looking to review or reference specific parts of the process. Each section is designed so that you can skip ahead if you already understand it, or focus on that specific part.

If you’re reading this guide as a PDF on a computer or digital device, text that has a black box around it is a hyperlink to a website. For example:

[http://www.mozilla.com/](http://www.mozilla.com/)

You can follow these links to learn more, but be mindful about following them on unsafe networks. Some of these sites may flag you for security violations. Follow them only on safe and trusted networks.

:[](plan)

:[](record)

:[](share)
