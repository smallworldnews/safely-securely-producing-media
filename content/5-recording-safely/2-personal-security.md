# 5.2 PERSONAL SECURITY

![Personal Security](images/personal_security_small.png)

The camera has been called more powerful than a gun, as the pen has previously been called mightier than the sword. Today cameras, whether mobile phones or photo/video cameras, can put a target on your back.

---

Observe these rules to keep yourself safe:

* **Cover your camera.**

* **Have a spotter watch your back.**

* **Get your shots and get out.**

* **Keep a card with dummy media.**

---
