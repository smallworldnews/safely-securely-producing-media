# 5. RECORDING SAFELY

## CHAPTER 05

---

**“Do No Harm”** is a principle of journalism, and it’s a rule to follow even if you don’t consider yourself a journalist. The images you create to tell your story can have information that can put you and others at great risk.

---
