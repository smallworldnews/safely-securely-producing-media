# 5.1 SUBJECT SECURITY

![Subject Security](images/subject_security_small.png)

When producing media in a repressive environment, visual recordings of people speaking out can be death sentences. You may receive informed consent, but you must still be concerned about the safety of those you record.

Trust your instinct. The best protection against putting a subject at risk is not to record visually identifiable information about them in the first place.

---

If you’re concerned about doing this, you can record interviews and testimony that protects subject security by following these simple rules:

* **Only include one eye and one ear in your shot.**

* **Shoot your subject with heavy backlighting.**

* **Shoot out of focus.**

* **Shoot only their hands.**

---
