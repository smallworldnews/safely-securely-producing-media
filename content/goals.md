# SMALL WORLD NEWS GOALS

Small World News focuses on developing the capacity of citizens to engage with the international community in crisis areas and conflict zones.

Our most recent project, Alive in Libya, showcased the potential of citizen media when combined with affordable digital technologies and professional training. As an organization our primary focus has been to guide local citizens through the entire process, from learning to produce professional media to distributing that content via social media and leveraging relevant technologies to broaden the impact.

We believe our unique body of expertise with media development in conflict areas such as Iraq, Afghanistan, and Libya makes us uniquely positioned to provide training to existing media professionals, human rights and civil society organizations, and independent citizens. In the past year, in addition to our own projects, Small World News has conducted training in Iraq, Afghanistan, India, Rwanda, Bahrain, Libya, and Uganda. Training subjects included: new media tools for civil society, SMS and mobile technology, training for journalists in new media and multimedia, training and advising in online security, training and deployment of online mapping tools.
