# 9.1 METHOD

Before you do anything else after you’ve recorded your story, get somewhere trusted and safe to secure the material you just created. You don’t want to get stopped and have your media deleted or lose it somewhere, such as when you’re at an Internet cafe.

---

## HAVE A PLAN FOR WHERE YOU'LL STORE YOUR MEDIA.

![](images/PlanContent.png)

 ---

## STORE YOUR MEDIA (SD CARDS, TAPES, FILM) SOMEWHERE SAFE.

![](images/PlanSafe.png)

---

## CLEAN YOUR CAMERA IF YOU NEED TO.

![](images/PlanClean.png)

---

## CHARGE YOUR BATTERIES.

![](images/PlanBatteries.png)

---

## PUT YOUR CAMERA BACK IN IT'S CASE.

![](images/PlanCase.png)

---
