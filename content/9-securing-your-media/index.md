# 9. SECURING YOUR MEDIA

## CHAPTER 09

---

Have a plan for where you’re going to go when you’re done recording. Whether it’s your home, a friends house, or a workspace, know of a space you can go to download your content.

---
