# 7.2 LIGHTING

Light is what makes your images visible. Lack of light will limit the visibility of the image, as will too much light.

Film your subject with the light source directly on the subject.

![Well Lit](images/LIGHTING-WELL_LIT-Medium.png)

Put yourself and your camera between the subject and the light source.

![Well Lit](images/LIGHTING-WELL_LIT.png)

In this example above, the light source is behind the camera and the subject is facing the light source.

---

When the light source is directly behind your subject, the subject will be too dark and difficult to see.

![Poorly lit](images/LIGHTING-BACK_LIT-Medium.png)

This technique may be used to hide a subject’s identity by preventing the camera from recording their visual identity.

![Poorly Lit](images/LIGHTING-BACK_LIT.png)

---

**PRO TIPS:**
If you are inside or have little to no lightsource you can have the subject face the window and place your camera between the subject and window/lightsource.

---
