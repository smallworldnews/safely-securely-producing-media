# 7.1 CREATING YOUR SHOT LIST

To get five shots that tell a story you need more than five shots. Having several options after recording your story will enable you to tell the best story possible.

We advise you write your story out, and turn the elements of the story into shots you think you can get. This is known as a shot list.

Think of multiple shots for each **Element of Your Story**. You can use the rule of thirds to include more than one **Element of Your Story** in a single shot.

---

Here’s a shot list template you can use to create your own shot list.

| SHOT TYPE | SHORT DESCRIPTION | STORY ELEMENTS INCLUDED |
|-----------|-------------------|-------------------------|
|.          |.                  |.                        |
|.          |.                  |.                        |
|.          |.                  |.                        |

On the next page we’ll take a look at the example story from Chapter 6 and apply the Elements of a Story to it. Then we’ll create a shot list from that.

----

**EXAMPLE STORY:**

A local human rights organization organized a peaceful protest over text message. Rashida has gathered with her friends to attend the rally in the capital this afternoon. She was here to voice the concerns of her family who could not come.

The protest called for the resignation of a local official. The police were there to maintain order and protect public buildings. A small unknown group instigated violence. This led to clashes between the protesters and the police and the burning of several vehicles.

## WHO?

* Rashida, her friends, the Police, Riotiers.

## WHAT?

* A protest calling for the resignation of a local official.

## WHERE?

* The Capital and surrounding environment.

## WHEN?

* In the afternoon.

## WHY?

* Because of concerns about the actions of a local official.

## HOW?

* A rally in the capital organized by text message.

---

In order to plan your shots well, you need to know the goal of your story. Most often the first question, **Who**, will define this for you. In this story, we have four possible Whos, but for the shot list below, we will use Rashida as our **Who**.

If you chose to use the police, or the rioters as your Who then the shot list would look different.

| SHOT TYPE    | SHORT DESCRIPTION                                                | STORY ELEMENTS INCLUDED     |
|--------------|------------------------------------------------------------------|-----------------------------|
| Establishing | The entire Capital building                                      | Capital                     |
| Establishing | The capital with the police out front and protesters approaching | Capital, Police, Protesters |
| Establishing | The protesters marching with a city landmark visible             | City Landmark, Protesters   |
| Long         | The police standing in front of the Capital                      | Capital, Police             |
| Long         | Violent protesters setting fires                                 | Violent Protesters          |
| Long         | Group of friends apart of the protest                            | Friends, Protesters         |
| Medium       | Interview with one of the friends                                | Rashida                     |
| Medium       | Piece to Camera, my report of what’s happening in the square     | Yourself                    |
| Medium       | Interview with peaceful protester about what they want           | Protester                   |
| Close-Up     | Friend Chanting                                                  | Friend                      |
| Close-Up     | Shot of an individual police officer guarding the capital        | Police Officer, Capital     |
| Close-Up     | Shot of an individual rioter                                     | Rioter                      |
| Detail       | The fire the rioters set                                         | Fire                        |
| Detail       | Shot of Rashida’s banner                                         | Banner                      |
| Detail       | Cell phone with protest text message displayed                                         | Cell Phone                      |

This shotlist is only a guide. Often you will choose many more than three shots per shot type.

---
