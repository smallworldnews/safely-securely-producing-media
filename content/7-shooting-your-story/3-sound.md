# 7.3 SOUND

If you can’t hear the sound you lose the context of your story. Your images can be properly framed and have (effective) lighting, but without decent sound they may not be impactful.

---

## NATURAL SOUND

![](images/natural_sound.png)

If it’s windy, your sound will not be audible. You must either point your camera out of the wind, or protect your mic. Shield the mic with your hand, or tape a card to the windy side.

---

## PEOPLE / CROWDS

![People and Crowds](images/People_Crowds.png)

Crowds and large groups make a lot of noise which makes your sound inaudible. Don’t film interviews with large crowds in the background. Point your camera away from the crowd, with the subject facing the crowd.

---

## MACHINES

![Machines](images/Machines.png)

Generators, air conditioners, refrigerators, and fluorescent lights are all machines that create harsh audio distractions. Turn off the machines if possible, or change your location if you can- noise from fluorescent lighting is usually unavoidable.

---

**PRO TIPS:**

If your camera has an audio level feature, check that your background noise is no more than one third the volume of your interview. Be sure that your audio level is not peaking constantly.

---
