# SECURE WEB BROWSING

Installing and using a secure and stable build of Firefox with **Tor** enabled is an important firs step to operating online.

**You can find specific instructions about this here:**

**Windows**

* [https://www.torproject.org/docs/tor-doc-windows.html](https://www.torproject.org/docs/tor-doc-windows.html)

**OSX**

* [https://www.torproject.org/docs/tor-doc-osx.html](https://www.torproject.org/docs/tor-doc-osx.html)

**Linux**

* [https://www.torproject.org/docs/tor-doc-unix.html](https://www.torproject.org/docs/tor-doc-unix.html)

---

You should check and verify that you’re properly using Tor before you proceed.

* [**https://check.torproject.org/**](https://check.torproject.org/)

---
