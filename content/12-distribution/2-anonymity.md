# 12.2 ANONYMITY

Tor can make it impossible to track  your social media accounts to your computer. But it’s possible to connect you to your social media accounts by the content that is on them. So we advise you to consider setting up and posting your content via multiple anonymous accounts if you’re concerned about your safety. You can then have this content distributed in a central location by a third party who is not at risk.

---
