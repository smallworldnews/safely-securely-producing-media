# 12. DISTRIBUTION

## CHAPTER 12

---

Great, well-distributed media can travel across the world in minutes. But you need to understand not only how distribution channels work, but how to properly post your content to make it easier for people to push it out to the widest possible audience.

---
