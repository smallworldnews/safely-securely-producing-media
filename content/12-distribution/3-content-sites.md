# 12.3 CONTENT SITES

Content hosting sites are a great way to get video directly onto the web and available for the world to view immediately. But the traffic can be tracked if sophisticated equipment is available to the state. This is a great option if you’re confident that you’re safe to post files directly, but it would be advisable to create anonymous accounts to post the content to. Share your anonymous accounts only with trusted contacts. Upload your content to them from different anonymous computers. This will anonymize you further, and give a single place for the world to find your content.

**Suggested Sites:**

* YouTube
* Flickr
* Blip.tv
* TwitPic

---
