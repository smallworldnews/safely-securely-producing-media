# 12.4 SOCIAL NETWORKS

No social networking sites are truly secure. By their nature, you’re making your information public. You can create fake accounts to use, but even that is not 100% secure.

---

**FACEBOOK**

To learn more about protecting yourself on Facebook review their latest policies on their site:

[http://www.facebook.com/privacy/explanation.php](http://www.facebook.com/privacy/explanation.php)

Be mindful that while your security settings are only as secure as your weakest link. To learn more, review the latest security guide at [http://socialmediasecurity.com/](http://socialmediasecurity.com/)

---

**TWITTER**

The way to get your information out quickly via twitter is to be aware of the different hashtags people use (e.g. #jan25 #feb17 #Afghan10) and tag your content appropriately.

Twitter is no more secure then any other social network, but the ease of creating new and different accounts can help keep yourself anonymous. The trade-off is that your voice and images may not be considered credible.

---
