# 8. CREATING CONTEXT

## CHAPTER 08

---

Providing context is key to recording an accurate, reliable story. Although this is done partly by creating powerful images that provide a sense of place and the participants and events, this is not enough. This section of the guide will discuss a variety of steps to follow to contextualize your story.

---
