# 8.2 OFF CAMERA NARRATION

When recording an important event, you often know more about the scene you record than you may capture with your camera. If you are shooting an event from too far away to create a clear shot, you may narrate for the camera the events you are seeing.

![long shot](images/2-longshot.png)

You can also use narration to inform the viewer of the time and location of your recording. However, this is not as reliable as images of place and time.

---

**POSSIBLE ELEMENTS TO NARRATE:**

* Tell the story.
* Establish place and time.
* Explain your purpose.
* Tell the viewer what you are seeing.
* Tell the viewer how what you are seeing makes you feel.

---
