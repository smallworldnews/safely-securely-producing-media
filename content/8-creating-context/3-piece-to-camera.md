# 8.3 PIECE TO CAMERA

![medium shot](images/3-mediumshot.png)

If you judge it safe to be on camera, performing a piece to camera is often the most effective way to tell your role in the story.

Being on camera shows you are willing to take personal responsibility for your words and your content. Your personal feeling about the content you may also be more clear when presented on camera, rather than as a narration.

---

![](images/personal_security.png)

*This can be an incredibly dangerous choice to make if you have existing risk factors.*

---
