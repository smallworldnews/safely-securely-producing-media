# 8.1 VISUAL CONTEXT

Accurate representation of time and place is key to recording a reliable story. If your goal is to report or observe events, you need to record the time and location of events as accurately as possible. If you wish to promote a cause this may not be as important to you. However, you can never be sure where your project will lead, so its best to record time and place when possible.*

---

## RECORD STREET SIGNS

![](images/StreetSigns.png)

---

## RECORD NEWSPAPERS

![](images/Newspapers.png)

---

## CORRECTLY SET YOUR CAMERA'S CLOCK

![](images/CameraClock.png)

---

## RECORD THE POSITION OF THE SUN

![](images/Sun.png)

---

## RECORD LANDMARKS

![](images/Landmarks.png)

---

![](images/personal_security.png)

*Recording the time and place can put you at risk, as it identifies you were there at that time. Only do this if you feel confident and safe about your security.*

---

## EXAMPLE OF VISUAL CONTEXT

![Establishing Shot with Context](images/VisualContext.png)

**1. STREET SIGNS**

Street signs and other local signage helps identify the location.

**2. CORRECTLY SET YOUR CAMERA'S CLOCK**

A properly set clock will help you organize your shots later, and the files will also be time-stamped for anyone else to verify as well.

**3. POSITION OF THE SUN**

The lighting in the shot also gives away what time of day it is. In this case with little or no shadows it is definitely mid day.

**4. LANDMARKS**

Any unique building helps certify a shot came from a specific place.

---
