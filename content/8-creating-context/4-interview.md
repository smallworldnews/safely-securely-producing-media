# 8.4 INTERVIEW

Interviews may offer the best opportunity for adding context to your story. By interviewing key participants in the story, you provide the audience a broader understanding of events.

![close up](images/4-closeup.png)

Interviews should add a variety of elements to the story. They humanize and provide multiple sources to increase the reliability of your story.

---

In order to produce strong interviews you must follow a series of rules:

**LOCATION**

* Your location should improve, not degrade, the impact of your interview.

**SOUND QUALITY**

* If your audience can’t hear the interview, it’s not an interview.

**ASKING QUESTIONS**

* Ask open-ended questions.
* Avoid yes-or-no questions.
* *“What else should I have asked you about?”*
* *“Is there anything else you would like to say?”*

**CHANGING YOUR APPROACH**

* If your questions don’t provide a satisfactory answer, change your approach.

**FOLLOW UP**

* Pursue intriguing answers with follow-up.
* Use follow-up questions if you need more information.

---
