# 13. CLOSING NOTES

## CHAPTER 13

---

Even if you think the photo you just published is the best thing you’ve done, there are always going to be plenty of areas you can improve in. Never assume you’re done learning. Producing multimedia is a continual learning process, so push yourself. If you have peers who you’re working with, push each other as a group to make your next story better then your last.

---
