# 13.2 PRACTICE MAKES PERFECT

Every story you photograph or film is going to make the next one you do better. Even if you think your work is poor, going out every day and following our guidelines will help you tell a story more effectively then the day before.

The more experience you have the better you’ll be at handling any situation you find yourself in. So get out there and keep at it.

---
