# 13.1 REVIEW YOUR WORK / PERSONAL CRITIQUE

Always review and personally critique all the work you produce yourself. This is the only way to improve your work.

---

Some questions to ask yourself are:

## WHAT DID I DO RIGHT?

## WHAT DID I DO WRONG?

## HOW CAN I DO BETTER NEXT TIME?

---
