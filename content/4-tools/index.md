# 4. TOOLS

## CHAPTER 04

---

Once you’ve decided on your medium, its time to select your tools. If your access to gear is limited, you may be using a tool you already have a lot of experience with.

<!-- more -->

However, it’s best to relearn your tools within the framework of this guide for the best results.

---
