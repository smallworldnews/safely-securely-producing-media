# 1.1 ARE YOU AT RISK?

---

## ARE YOU LIVING IN A DICTATORSHIP?

## IS CORRUPTION A PROBLEM?

## ARE POLICE UNRESPONSIVE TO THE CONCERNS OF CITIZENS?

If your answers to any of these questions is Yes then you’re likely to be at risk. Be mindful of the chances you might be taking with your life and as well as others’ lives.

---

## TYPES OF RISK

Risk can come from many places. Be mindful of your surroundings and avoid interacting with anyone you don’t trust.

## 1 - DIRECT ENEMIES

Police, Military, Gangs, Political Opponents.

## 2 - INDIRECT ENEMIES

People whose actions may harm you.

## 3 - ENVIRONMENTAL

Fires, explosives, weapons, and any other situational dangers.

## 4 - PERSONAL

Actions you take may harm yourself and others.

---

![Long Shot Risks](images/2-longshot_risknumbers.png)

![Medium Shot Risks](images/3-mediumshot_risknumbers.png)

![Detail Shot Risks](images/4-closeup_numbers.png)


---
