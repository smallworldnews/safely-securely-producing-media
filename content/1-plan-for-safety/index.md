# 1. PLAN FOR SAFETY

## CHAPTER 01

----
![Image Test in Chapter Page](images/4-closeup_numbers.png)

---

*"Tell the truth and run."*

This is a famous phrase in journalism, but we hope this guide will assist you not just to run, but also to hide. To do this effectively you first have to understand who you are hiding from and why.

---
