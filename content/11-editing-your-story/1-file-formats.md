# 11.1 FILE FORMATS

![](images/FileFormat_AVI.png)

## .AVI

A format for Windows, supported by all the most popular web browsers. Not always possible to play on non-Windows computers.

---

![](images/FileFormat_3GP.png)

## .3GP / .3g2

A format for 3G cell phones. .3GP is used on GSM phones. .3G2 is for CDMA phones.

---

![](images/FileFormat_WMV.png)

## .WMV

A format on the Internet, but Windows Media movies cannot be played on non-Windows computers without additional software.

---

![](images/FileFormat_MOV.png)

## .MOV

A format for Macs. It can only be played on a Windows computer with additional software.

---
**VIDEO PLAYER SUGGESTION:**

If you’re having trouble opening or viewing any file formats, we suggest you install VLC player. It is a freely available video player that supports playback of a large number of formats.

* [VLC Player](http://www.videolan.org/vlc/)

---
