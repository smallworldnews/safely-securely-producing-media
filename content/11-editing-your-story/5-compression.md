# 11.5 COMPRESSION

Now that you’re finished with your video file, let’s discuss compression methods to post it online. We’re giving you two methods to follow depending on your connection speeds. You can adjust these yourself depending on your own needs.

**FAST CONNECTION:**


| VIDEO SETTINGS   |                               |
|------------------|-------------------------------|
|           Codec: | H.264                         |
|      Frame rate: | 24, 25, or 30 FPS             |
|       Data rate: | 2000 kbps (SD) 5000 kbps (HD) |
|      Resolution: | 640x480 (SD) 1280x720 (HD)    |
|   Deinterlacing: | NO (SD Card) YES (Tape)       |
|**AUDIO SETTINGS**|                               |
|           Codec: | AAC                           |
|       Data rate: | 320 kbps                      |
|     Sample rate: | 44.1 kHz                      |

**SLOW CONNECTION:**

|  VIDEO SETTINGS  |                               |
|------------------|-------------------------------|
|           Codec: | H.264                         |
|      Frame rate: | 24, 25, or 30 FPS             |
|       Data rate: | 1000 kbps (SD) 2500 kbps (HD) |
|      Resolution: | 320x240 (SD) 960x540 (HD)     |
|   Deinterlacing: | NO (SD Card) YES (Tape)       |
|**AUDIO SETTINGS**|                               |
|           Codec: | AAC                           |
|       Data rate: | 128 kbps                      |
|     Sample rate: | 44.1 kHz                      |


The important variable here is your Data Rate. The lower your data rate, the smaller your file. However, using a higher resolution with a lower data rate will create a very poor image. Be sure to adjust these together.

---
