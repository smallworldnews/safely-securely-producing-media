# 11.3 TRANSITIONS - VIDEO

The only video transition you should use starting out is the simple cross fade. You should not use any swirly transitions or obnoxious twists. Cross fades should only be used a few times, repeatedly using them is seen as amateurish. If your story is not changing locations, or you do not want to indicate the passing of time, you should avoid cross fades.

Depending on the editing software you use, this will vary. Look for a way to add a transition to the two clips you’d like to cross fade, and add the cross fade at the point where they come together. This concept is fairly universal.

---
