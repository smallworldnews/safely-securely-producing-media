# 11.4 TITLING

Adding text to your content can clarify missing information such as location, time, date, and the names of people. You can also add text or images to brand the video as yours, or a production team you work with.

**EXAMPLE:**

![Titling](images/Titling.png)

---
