# 11. EDITING YOUR STORY

## CHAPTER 11

---

Adding additional information and lightly editing your content is not essential before distribution. It does, however, add a lot of value for your audience. This chapter is for people who want to take the first steps into editing their content.

---
