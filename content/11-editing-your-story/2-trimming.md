# 11.2 TRIMMING

You might have some video at the beginning of a clip that puts someone at risk, or maybe you forgot to stop recording at the end of a clip. Trimming the beginning and the ending of a clip off can make a clip safer and it expedite your upload times.

Focus on trimming your clips to have a better beginning point and ending point. If it “feels wrong” or “looks weird”, then it probably is. A good general rule is to give a clip 3 seconds of lead-in and 2 seconds of lead-out.

---

## ORIGINAL FILE

Has extraneous footage that you’d like to get rid of.

![Original Clip](images/Trimming_Original.png)

---

## EDIT SELECT

You select just the content you’d like.

![Edit Clip](images/Trimming_Edit.png)

---

## NEW FILE

Your new file just has the footage you’d like.

![New Clip](images/Trimming_New.png)

The interview from beginning to end with good composition is the interesting part. Keep this to share with the world.

---

**PRO TIPS:**

During recording, adding 10 seconds of lead-in and 10 seconds of lead-out will ensure that you have covered your action or interview for editing later.

---
