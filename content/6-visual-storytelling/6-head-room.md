# 6.6 HEAD ROOM

Head room should be considered while shooting Medium and Close Up shots. Following these two rules together will give you good head room.

![headroom examples](images/headroom.png)

**HEAD ROOM RULES:**

1. Keep your Subjects eyes on the upper rule of thirds line, this is known as the eye line.

2. Keep your subjects entire head in the frame.

---
