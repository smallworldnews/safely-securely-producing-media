# 6.3 FIVE BASIC SHOTS

Narrative, documentary and news videos are all constructed from a number of basic shots. The human eye can focus and refocus in real time, effectively framing and re-framing the object of interest.

In video, different-sized shots are used to create the new frames.

How do you know what shots to use? Let’s review the five basic shots and what they’re most effective at.

---

## ESTABLISHING SHOT

![establishing shot](images/1-establishingshot.png)

Shows the location / scene of a story. Gives a sense of place.

----

## LONG SHOT

![long shot](images/2-longshot.png)

Showcases the characters and how they interact with the location.

----

## MEDIUM SHOT

![medium shot](images/3-mediumshot.png)

Directs the audience to focus entirely on one or two characters, may not provide an understanding of the location.

----

## CLOSE-UP SHOT

![Close up shot](images/4-closeup.png)

Forces the audience to focus entirely on a single character, encourages the emotion of the character.

----

## DETAIL SHOT

![Detail shot](images/5-detail.png)

Showcases an interesting detail, often focusing directly on an important action.

----

## OBFUSCATION

If you need to conceal or hide the identity of someone in an image, you’ll need to know how to alter the image so they’re unrecognizable.

### SHOOT OUT OF FOCUS

![](images/4-closeup-obfuscation.png)

The easiest way to do this is to adjust the focus on your camera so that the persons face is blurry beyond recognition while you’re filming. This is the safest way to hide someones identity.

### POST PRODUCTION BLURRING

![](images/4-closeup-pixel.png)

If you need to do this after you already filmed the interview you can use computer software to do this. *(note: This is still not possible in iMovie without 3rd party software.)*

You can find tutorials about this at the following places:

**Windows**
* [http://www.youtube.com/watch?v=n_X8jWng9aM]()

**OSX**:
* [http://www.youtube.com/watch?v=nO-kDGiMmZU]()

---

![](images/subject_security.png)

This media will be at risk until you safely store your media. See [Chapter 9](/content/9-securing-your-media/index.md) for more details.

-----

## FIVE BASIC SHOTS APPLIED

Here we have the five basic shots as they are used in our story from earlier.

![images/montage.png](images/Montage.png)

**STORY DESCRIPTION:**
A local human rights organized a peaceful protest over text message. Rashida has gathered with her friends to attend the rally in the capital. She was here to voice the concerns of her family who could not come. The protest called for the resignation of a local official. The police were there to maintain order and protect public buildings. A small unknown group instigated violence. This led to clashes between the protesters and the police and the burning of several vehicles.

---
