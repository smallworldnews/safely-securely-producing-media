# 6.1 DECIDE YOUR STORY

Once you know your **story**, the second most important decision is what you want in your **shot**. If you don’t know what you want to shoot, or why you are shooting it, it’s impossible to improve.

---

You can start deciding ion your shot by considering the **elements of a story** that you want to include.

* **Who** are the characters you want to highlight? Who is important in this shot?
* **What** is the goal of your story? What is important in this shot?
* **Where** is the story taking place?
* **How** do the events occur?
* **Why** is this image important?

---
