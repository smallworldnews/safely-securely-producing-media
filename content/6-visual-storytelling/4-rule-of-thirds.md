# 6.4 RULE OF THIRDS

How do you frame your shot for maximum impact? The rule of thirds is a guideline to help you create appealing shots.

![Rule of thirds grid](images/rule_of_thirds.png)

This grid is known as the Rule of Thirds. It cuts the frame into thirds vertically and horizontally. This creates four points of interest within the frame. These four points are where you should place the important **elements of your story** when you’re framing them.

Be aware of all four points as you frame your shots and try to include other important or interesting markers on each available intersection. Strive to have four points of interest in all of your shots.

-----

## ESTABLISHING SHOT

![Establishing Shot](images/1-establishingshot_3rds.png)

An establishing shot provides a sense of place.

**Point 1.** The government building shows **where** and **why**.

**Point 2.** The mosque provides a sense of place, or **where**.

**Point 3.** The police are an important **who** in this story.

**Point 4.** The protesters are another important **who**.


The establishing shot tells us this story is about a protest at a government building in an Islamic area.

-----

## LONG SHOT

![Long Shot](images/2-longshot_3rds.png)

A Long shot highlights the characters in the space.

**Point 1.** The police are an important **who** in this story.

**Point 2.** The protesters are another important **who**.

**Point 3.** The police are an important **who** in this story.

**Point 4.** The banner highlights what the protesters **want** and **why** they protest.


The long shot introduces the main characters from the protest and there demands. It also highlights the relationship between the police and protesters.

-----

## MEDIUM SHOT

![Medium Shot](images/3-mediumshot_3rds.png)

A medium shot focuses on a specific character.

**Point 1.** The mosque provides a sense of place, or **where**.

**Point 2.** A protester, **who** speaks to the camera, explains **why** events in Point 3 are happening.

**Point 3.** These protesters are an important **who** in this story.

**Point 4.** See Point 2.

This medium shot focuses on a specific protester explaining **why** she is at the protest. In the background, you again see where it is happening. The middle ground between the mosque and the protester shows what she is explaining.

----

## CLOSE-UP SHOT

![Close-up Shot](images/4-closeup_3rds.png)

A close-up shot highlights the emotion of a specific character.

**Point 1.** A protester, **who** speaks to the camera, explains **why** he is there.

**Point 2.** Nothing. A better shot might show the protest sign, **explaining** what the protest is about.

**Point 3.** See Point 1.

**Point 4.** See Point 2.

The close-up shot highlights the emotion of a protester explaining why he is at the protest. Though you may not use all four points effectively, capturing emotion and feeling is your primary objective - not framing a shot with a perfect use of all four points.

-----

## DETAIL SHOT

![Detail Shot](images/5-detail_3rds.png)

A detail shot highlights a specific action or detail.

**Point 1.** Mobile phone depicting **how** the protest occurred.

**Point 2.** Nothing.

**Point 3.** See Point 1.

**Point 4.** See Point 2.

The detail shot appears basic, but is a very important shot. It clarifies details that are lost in wider shots. Be sure to place the most important detail on one of the four points.

---
