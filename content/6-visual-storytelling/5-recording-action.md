# 6.5 RECORDING ACTION

![Recording Action](images/RECORDING_AN_ACTION.png)

Timing is just as important as framing. You want to be sure you record entire actions in your video. Recording a complete action can tell a story with a single shot.

When showing an action you’ll want time both before it begins and after it to understand it. To do this it’s best to record for 10-20 seconds before and after your point of interest. No matter how brief the action is shots should be at least 30 seconds.

----

**PRO TIPS:**

You don’t need to record constantly, but sometimes recording for one, two or even five minutes is necessary to record an action in it’s entirety.

It’s important to review and share your content in a timely fashion. Don’t shoot too much material, this will create a barrier to sharing your stories quickly.

---
