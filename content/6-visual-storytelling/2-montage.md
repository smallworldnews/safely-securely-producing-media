# 6.2 MONTAGE

A visual story is a series of shots, when presented together over time it’s called a montage.

![Montage](images/Montage.png)

**STORY DESCRIPTION:**

A local human rights organized a peaceful protest over text message. Rashida has gathered with her friends to attend the rally in the capital. She was here to voice the concerns of her family who could not come. The protest called for the resignation of a local official. The police were there to maintain order and protect public buildings. A small unknown group instigated violence. This led to clashes between the protesters and the police and the burning of several vehicles.

---
