# 6. VISUAL STORYTELLING

## CHAPTER 06

---

The importance of story was introduced in [Chapter 2](/content/2-what-a-story-is/index.md), but telling a story visually requires an understanding of the visual medium. Chapter 6 introduces the idea of Visual Storytelling.

---
