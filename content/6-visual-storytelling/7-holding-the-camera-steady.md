# 6.7 HOLDING THE CAMERA STEADY

Smaller cameras mean you are more likely to keep your camera with you, but they can be difficult to keep steady. The weight of larger cameras makes them easier to hold steady. To assist you, here are some tips about holding your camera steady.

![Good posture](images/holdingcamera-good.png)

It’s best to use your body to brace the camera, the individual above uses the side of their body to brace the camera, while centering it in front of them.

**PRO TIP:**

Remember to breathe. You may be tempted to hold your breath to keep the camera from moving, but its best to set your shot, breathe, and when you have a steady shot, begin recording.

---
