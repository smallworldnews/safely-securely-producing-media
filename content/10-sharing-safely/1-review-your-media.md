# 10.1 REVIEW YOUR MEDIA

Now that your content is safe and secure, you need to begin reviewing it.

---

![](images/4-closeup.png)

Ask yourself: Is my content good? Was I ethical? If so, proceed to the next section.

---

![](images/4-closeup-BAD.png)

If not, we strongly suggest you delete your content. Having it on hard drives, in email accounts, or anywhere connected to you could put you and others at great risk.

---

**ARE YOU PUTTING ANYONE AT RISK?**

Maybe you forgot to delete an interview with someone who asked you to, or perhaps you didn’t ask someone permission to use a close up of them. Think about the potential risk you’re taking. You may be putting your own life and someone else’s life in danger.

---

**GOOD CONTENT?**

It’s not worth risking lives for bad content. If you’re media is confusing, poorly shot, or just plain boring don’t risk posting it online. Use it as a learning experience. Review it and think how you could improve it if you shot it again, then delete the media.

---

**CONSENT GIVEN?**

If you have consent and you think the subject was making informed consent, you should feel free to post it online. If you don’t have consent then you should delete the media. If you have consent but your instincts tell you that the subject was not making informed consent, then you should obfuscate the footage.

---

**OBFUSCATE**

You or someone on your team will need to know how to use photo or video software to do it. If you’d like to learn how to do this, you can learn more about it in Chapter 6.3. If this seems like too much for you and your team, then you should delete the media.

----

**PRO TIPS:**
Look for the most representative content for the story you need to tell and delete the rest.

---
