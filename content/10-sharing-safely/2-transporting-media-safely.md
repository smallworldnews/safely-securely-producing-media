# 10.2 TRANSPORTING MEDIA SAFELY

Transmitting your content will be among the highest security risks you take in this process. We’ll walk you through a couple of methods for getting your content online, and we’ll explore digital and analog methods.

---

![](images/subject_security.png)

*No file transfer service is without fault or 100% safe. By encrypting your data before uploading it, you can avoid the primary security issues of note. You can encrypt the data source before sending to your support team. http://truecrypt.org/*

---

## DIGITAL

Storing and sending your files over the Internet or any computer network is doing so digitally.

## ANALOGUE

Storing and sending your files on electronic devices that are transported physically - by moving the film, SD cards, etc. - is analog distribution.

Let’s review some of the different options available.

---

**DIGITAL DIRECT TRANSFERS**

![Direct File Shring](images/FIleSharingDirect.png)

Sending your content over secure servers you own or are owned and operated by your trusted contacts is a great option. This keeps the content private even when it’s finished transmitting. It’s important to have trusted colleagues, however, who can take that content for you and be sure that it is posted online and promoted.

Essential Security Features:

* FTP Software: Use FTPS
* Email Software: PGP

Other services that have these features could be good options.

Current Suggested Services:

* FTP - Filezilla
* Email - Thunderbird
* Email - Hushmail

---

**DIGITAL FILE SHARING SITES**

![Digital File Shring](images/FIleSharingCloud.png)

File sharing sites are another option to send files directly to individuals helping you from outside of your location. Be sure to keep your file names as anonymous as possible while doing this. Come up with nondescript file names, e.g. “Clip_001” “File_001”, with the # adding up for each file you send.

Essential Security Features:
* SSL
* AES-256 encryption

If you find another service that has these options it is like to be an acceptable option.

* DropBox
* SpiderOak
* YouSendIt

---

**ANALOG**

Analog methods are sometimes your only option to getting content out of the country. There are a couple of tools we’ll review, but the thought behind all of them is simple: find non-traditional places where you can store/hide digital files. Hiding things in unexpected places and using equipment that will make you look unsophisticated is the challenge.

---

**MICRO SD**

![MicroSD Cards](images/Format_MicroSD.png)

---

**MP3 PLAYER**

![MP3 Player](images/Format_MP3Player.png)

---

**NONTRADITIONAL**

![Non Traditional](images/Format_NonTraditional.png)

---

Keep in mind people will be looking for these, and that the more popular and common these methods become, the more likely they are to be searched for. Push yourself to be as creative as possible in devising these methods.

---

![Subject security](images/subject_security.png)

*These methods can also be a good use for Truecrypt. You can encrypt the data source before sending to your support team. http://truecrypt.org/*

---
