# 10. SHARING SAFELY

## CHAPTER 10

---

You’ve taken a lot of risk to produce this media, so be sure to follow through with it now that you’ve created it. Once you’re in a safe space, prepare your media to share it with the world. Even the best photograph in the world is meaningless if the world can’t see it. So lets review the essential steps to keep you and your characters safe and secure so you can focus on assembling your story, and share it with the world.

---
