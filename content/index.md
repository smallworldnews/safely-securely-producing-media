# SAFELY & SECURELY PRODUCING MEDIA

There are many great guides to help you securely send media files online. But we find they are mostly text-heavy instructions for dealing with technical or security concerns surrounding the posting of content online. In response, we’ve decided to create a guide combining this information with a clear visual walk through on producing high-quality media. Our primary goal is to not only teach you how to share your story with the world safely, but also help you clarify what that story is and communicate it more effectively.
