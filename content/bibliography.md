# BIBLIOGRAPHY

**How to Communicate Securely in Repressive Environments**

* [http://irevolution.net/2009/06/15/digital-security/](http://irevolution.net/2009/06/15/digital-security/)

**WITNESS Video Advocacy Training Guide**

* [http://witnesstraining.wordpress.com/](http://witnesstraining.wordpress.com/)

**Sophos Facebook Security Best Practices**

* [http://www.sophos.com/en-us/security-news-trends/best-practices/facebook.aspx](http://www.sophos.com/en-us/security-news-trends/best-practices/facebook.aspx)

**W3 Schools Media Formatting**

* [http://www.w3schools.com/media/media_videoformats.asp](http://www.w3schools.com/media/media_videoformats.asp)

**Wikipedia Pages**

* [http://en.wikipedia.org/wiki/Category:Journalism](http://en.wikipedia.org/wiki/Category:Journalism)
* [http://en.wikipedia.org/wiki/Five_Ws](http://en.wikipedia.org/wiki/Five_Ws)
