# 2.4 DEMONSTRATION OF QUESTIONS

![Medium shot](images/3-mediumshot_numbersGoals.png)

## 1. WHO

The female protester addressing the camera is **who**.

## 2. WHAT

A car destroyed by protesters is **what**.

## 3. WHERE

The mosque and unique landmarks show us **where**.

## 4. WHEN

The specific day cannot be determined. The lighting tells us **when**.

## 5. WHY

The female protester explains **why**.

## 6. HOW

The female protester explains **how**. The other protesters show us **how**.

---
